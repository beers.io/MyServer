import Vapor

@main
public struct MyServer {
    public static func main() async throws {
        let webapp = Application()
        webapp.get("greet", use:Self.greet)
        webapp.post("echo", use:Self.echo)
        try webapp.run()
    }
    
    static func greet(request:Request) async throws -> String {
        return "Hello from Swift Server"
    }
    
    static func echo(request:Request) async throws -> String {
        if let body = request.body.string {
            return body
        }
        return ""
    }
}
